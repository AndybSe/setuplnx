#!/bin/sh
sudo apt-get update
sudo apt-get install nginx
#Install Node Js

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install npm
sudo npm install pm2 -g
#install git
sudo apt-get install git
sudo apt-get install curl openssl libssl-dev
#install build tools
sudo apt-get install -y build-essential
#firewall
sudo ufw allow OpenSSH
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow http
sudo ufw allow https
sudo ufw enable
sudo ufw status verbose
#certbot/letsencrypt
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install python-certbot-nginx
#.NET Core https://www.microsoft.com/net/download/linux-package-manager/ubuntu17-10/runtime-current
wget -q https://packages.microsoft.com/config/ubuntu/17.10/packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
sudo apt-get update
sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install aspnetcore-runtime-2.1
sudo apt-get install aspnetcore-runtime-2.1
#start nginx
sudo systemctl start nginx 
#vsts build agent
sudo apt install unzip
